$(document).ready(() => {
  let mypromise101 = new Promise((resolve, reject) => {
    $.ajax({
      url:"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=MSFT&apikey=demo",
      success: (response) => {
        console.log("success");
        console.log(response);
        resolve(response);
      },
      error: (xhr) => {
        reject(xhr.status + " " + xhr.statusText);
      }
    });
  });
  console.log("this is after ajax, but does not wait for it to execute");
  mypromise101.then( (result) => {
    let dataHTML = "<br><table border= '1px'>";
    $.each( result, ( key, val) => {
      dataHTML = dataHTML+"<tr><td>"+key+"</td></tr>";
      $.each( val, ( k, v) => {
        dataHTML = dataHTML+"<tr><td>"+k+"</td><td>"+v+"</td></tr>";
      });
    });
    dataHTML += "</table>";
    $("#div3").html(dataHTML);
  });
  mypromise101.catch( (error_msg) =>{
    console.log(error_msg);
  });
});
