//setting on click on calculator table UI
document.getElementById("calt").onclick = concatstr ;

//Global Variables
var ans="";
var din;

// set display
function setdin(newdin){
  document.getElementById('din').value = newdin ;
}

//get expression
function getdin(){
  return document.getElementById('din').value;
}

//add passed value to expression string
function addToExpression(p){
  din = getdin() + p;
  setdin(din);
}

//function called when calculator table is clicked
function concatstr() {
  //getting activeElement, i.e the clicked element into local variable
  var t = document.activeElement;
  var p = t.value;

  //add clicked numbers and operators into expression string
  if (t.classList.contains("nao")) {
    addToExpression(p);
  }// else goto back, clear, subeq or ans
  else if (t.classList.contains("fnas")) {
    switch (t.id) {
      case "back": // delete last input
        din = getdin().slice(0, -1);
        setdin(din);
        break;
      case "clear": // clear input text expression
        din = "";
        setdin(din);
        break;
      case "subeq": // try to evaluate
        if (getdin()==="") {
          alert("Nothing to calculate")
        } else {
          try {
            ans = eval(getdin());
            setdin(ans);
          } catch (e) {
            alert(e.message);
          } finally {
          }
        }
        break;
      case "ans": // add previous answer(if any) to input expression
        if (ans==="") {
          alert("No previous answer")
        } else {
          addToExpression(ans);
        }
        break;
      default:
        alert("something is wrong");
        break;
    }
  }
}
