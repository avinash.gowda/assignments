// flag to comfirm validity of each formfield
let validStatus = { fname:0, mname:0, lname:0, mnumber:0, email:0, address:0 };
// error message for each formfield
let errorMsg = { fname:"Please enter letter only", mname:"Please enter letter only", lname:"Please enter letter only", mnumber:"Please enter 10 digit number", email:"email incorrect", address:"Use alphabets, numbers and characters[ / . , ) ( - _ ] only", button:"Please complete or correct the form" };
// on document ready start
$(document).ready(function(){
  //Setting event Listeners
  $("input").blur(function(){textValidate(this);});
  $("textarea").blur(function(){textValidate(this);});
  $("button").click(function(){
    let validStatusChecksum = 0;
    Object.values(validStatus).forEach(function(v,k){
      validStatusChecksum += v;
    });
    if ( validStatusChecksum == 6){
      ajaxSubmit();
      $(this).next().css({"display":"none"});
    }
    else {
      $(this).next().html(errorMsg["button"]);
      $(this).next().css({"display": "block"});
    }

});
// function to validate formfields
// blured element is passed here as 'ele'
function textValidate(ele) {
  // get 'name' attribute to identify element in ename
  let ename = $(ele).attr("name");
  // flag used to validity status withing function only
  let isValid = 0;
  // when blured element is empty
  // else check validity for input and textarea
  if ($(ele).val() == "") {
    isValid = 5;
  } else {
    if (ename != "address") {
      isValid = $(ele)[0].checkValidity();
    } else {
      let regex = /^[a-zA-Z0-9 /.,)(-_]{1,500}$/;
      isValid = regex.test($(ele).val());
    }
  }
  // if formfield is empty, do not display error_message or any different css, just flag it as not valid
  // else display error message on invalid and undo on valid
  if (isValid == 5) {
    validStatus[ename]=0;
    $(ele).css("background-color", "white");
    $(ele).next().css({"display":"none"});
  } else if (isValid) {
    $(ele).css("background-color", "#bfffc1");
    $(ele).next().css({"display":"none"});
    validStatus[ename]=1;
  } else {
    $(ele).css("background-color", "#ffbfc1");
    $(ele).next().html(getrrorMsg());
    $(ele).next().css({"display": "block"});
    validStatus[ename]=0;
  }
  // logs validity flags
  console.log(Object.values(validStatus));
  // helper function to get error message for specified formfield
  function getrrorMsg() {
    if ($(ele)[0].validity.patternMismatch || ename == "address") {
      return errorMsg[ename];
    } else {
      return $(ele)[0].validationMessage;
    }
  }
}
// submit using ajax
function ajaxSubmit() {
  // get form data into formData object
  let formData = { fname:'', mname:'', lname:'', mnumber:'', email:'', address:'' };
  $("input").each(function(){
    formData[$(this).attr("name")] = $(this).val();
  });
  formData[$("textarea").attr("name")] = $("textarea").val();
  // log formData
  console.log(formData);
  // ajax
  $.ajax({ type: "POST", url: "ajaxHandle.php", data: formData, success: function(resMsg){ $("#success").html(resMsg); }});
}

});
