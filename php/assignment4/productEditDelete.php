<?php
// handles edit and delete
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // connection class file
  include_once('dbconn.php');

  class productEditDelete
  {
    // returns db connection
    public function getDBConnection()
    {
      $conndb = new dbconn;
      return $conndb->conndb();
    }
    // redirect back to product list
    public function gotoProductList()
    {
      header("Location: listProduct.php");
    }
    // updates product in db
    public function pedit($pcodeOld,$pcode,$pname,$price,$pactive)
    {
      $conndb = $this->getDBConnection();
      $editProductq = 'update product set product_code = "'.$pcode.'", name = "'.$pname.'", price = '.$price.', is_active = "'.(($pactive == 'Yes')?$pactive:'No').'" where product_code = "'.$pcodeOld.'"';
      if ($conndb->query($editProductq)) {
        $conndb->close();
        $this->gotoProductList();
      } else {
        echo "error, unable to submit edited product";
      }
    }
    // deletes product in db
    public function pdelete($pcode)
    {
      $conndb = $this->getDBConnection();
      $deleteProductq = 'delete from product where product_code = "'.$pcode.'"';
      if ($conndb->query($deleteProductq)) {
        $conndb->close();
        $this->gotoProductList();
      } else {
        echo "error, unable to delete product";
      }
    }
    // get edit data through form
    public function getEditData($pcode)
    {
      $conndb = $this->getDBConnection();
      $getEditDataq = 'select * from product where product_code = "'.$pcode.'"';
      if ($product = $conndb->query($getEditDataq)) {
        $conndb->close();
        $product = $product->fetch_row();
        echo '
        <!DOCTYPE html>
        <html lang="en" dir="ltr">
          <head>
            <meta charset="utf-8">
            <title>Edit Product</title>
            <link rel="stylesheet" href="w3.css">
          </head>
          <body>
            <header class="w3-container w3-border-bottom">
              <h2>Edit Product</h2>
            </header>
            <br>
            <main class="w3-container">
              <form id="productEdit" class="w3-container" action="productEditDelete.php" method="post">
                <h4>fill in new product details:</h4>
                <input type="hidden" name="product_code_old" value="'.$product[0].'">
                <input class="w3-section" type="text" name="product_code" value="'.$product[0].'" placeholder="Product code" required><br>
                <input class="w3-section" type="text" name="pname" value="'.$product[1].'" placeholder="Product name" required> <br>
                <input class="w3-section" type="text" name="price" value="'.$product[2].'" placeholder="Price" required><br>
                <input class="w3-section" id="is_active" type="checkbox" name="is_active" value="Yes" '.(($product[3] == "Yes")?"checked":"").'> <label for="is_active">Active Product</label><br>
                <button class="w3-btn w3-blue w3-section" form="productEdit" type="submit" name="submit" value="Submit"> Submit </button>
              </form>
              <h4 class="w3-text-red">*all fields are required</h4>
            </main>
          </body>
        </html>
        ';
      }
      else {
        echo "error, unable to retrieve Edit Data";
      }
    }
  }
  $ped = new productEditDelete;
  // product list to edit
  if (isset($_POST['edit'])) {
    $ped->getEditData($_POST['edit']);
  }
  // edited data to db
  elseif (isset($_POST['submit'])) {
    $ped->pedit($_POST['product_code_old'],$_POST['product_code'],$_POST['pname'],$_POST['price'],$_POST['is_active']);
  }
  // product list to delete
  elseif (isset($_POST['delete'])) {
    $ped->pdelete($pcode);
  }


}
?>
