<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Product List</title>
    <link rel="stylesheet" href="w3.css">
  </head>
  <body>
    <!-- common navigation section -->
    <?php include 'merchantIndex.php'; ?>
    <header class="w3-container w3-border-bottom">
      <h2>Product List</h2>
      <?php
      // php class listProduct is defined here
      // including dbconn.php for db connection
      include_once('dbconn.php');
      class listProduct
      {
        // connects to db and return mysql result
        public function getProductList()
        {
          $conndb = new dbconn;
          $conndb = $conndb->conndb();
          $getProductsq = 'select * from product';
          if ($productsList = $conndb->query($getProductsq)) {
            $conndb->close();
            return $productsList;
          }
          else {
            return 0;
          }
        }
        // calls getProductList and parses mysql result into table rows
        // rows get echoed into table, where class object is created
        public function getProductListHtmlTable()
        {
          $productsList = $this->getProductList();
          if ($productsList !== 0) {
            $trs = "<tr><td>"; $trm = "</td><td>"; $tre = "</td></tr>"; $bttns = '<button class="w3-btn w3-block w3-'; $orange = 'orange'; $red = 'red'; $bttnc = '" type="submit" name="'; $bttnm = '" value="'; $bttne = '">'; $bttnee = '</button>';
            $productsListTable = "";
            while ($productDetail = $productsList->fetch_row()) {
              $productsListTable .= $trs.$productDetail[0].$trm.$productDetail[1].$trm.$productDetail[2].$trm.$productDetail[3].$trm.$bttns.$orange.$bttnc."edit".$bttnm.$productDetail[0].$bttne."edit".$bttnee.$trm.$bttns.$red.$bttnc."delete".$bttnm.$productDetail[0].$bttne."delete".$bttnee.$tre;
            }
            $productsList->close();
            echo $productsListTable;
          } else {
            echo "error, something went wrong";
          }
        }
      }
      ?>
    </header>
    <main class="w3-container">
      <form class="w3-container" action="productEditDelete.php" method="post">
        <table class="w3-table-all">
          <tr><th>Product code</th><th>Product name</th><th>Price</th><th>Active</th><th colspan="2">Edit/Delete</th></tr>
          <?php
          $productsList = new listProduct;
          // list is echoed here
          $productsList->getProductListHtmlTable();
          ?>
        </table>
      </form>
    </main>
  </body>
</html>
