<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Product Entry</title>
    <link rel="stylesheet" href="w3.css">
  </head>
  <body>
    <?php include 'merchantIndex.php'; ?>
    <header class="w3-container w3-border-bottom">
      <h2>Product Entry</h2>
    </header>
    <br>
    <main class="w3-container">
      <?php
      // php class productEntry is defined here
      // run rest of code only on POST request
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        echo "<br>";
        // including dbconn.php for db connection
        include_once('dbconn.php');
        class productEntry
        {
          public function insertProduct($pcode,$pname,$price,$pactive)
          {
            $conndb = new dbconn;
            $conndb = $conndb->conndb();
            $insertProductq = 'insert into product ( product_code, name, price, is_active) values ("'.$pcode.'","'.$pname.'",'.$price.',"'.$pactive.'");';
            if ($conndb->query($insertProductq)) {
              echo "<h4>Product '".$pname."' was added. you can continue adding more product.</h4>";
            } else {
              echo "somthing wrong in db connection";
            }
            $conndb->close();
          }
        }
        $pe = new productEntry;
        $pe->insertProduct($_POST["product_code"],$_POST["name"],$_POST["price"],(($_POST["is_active"] == "")?"No":$_POST["is_active"]));
      }
      ?>
      <!-- form -->
      <form id="productEntry" class="w3-container" action="productEntry.php" method="post">
        <h4>fill in new product details:</h4>
        <input class="w3-section" type="text" name="product_code" value="" placeholder="Product code" required><br>
        <input class="w3-section" type="text" name="name" value="" placeholder="Product name" required> <br>
        <input class="w3-section" type="text" name="price" value="" placeholder="Price" required><br>
        <input class="w3-section" id="is_active" type="checkbox" name="is_active" value="Yes"> <label for="is_active">Active Product</label><br>
        <button class="w3-btn w3-blue w3-section" form="productEntry" type="submit" name="submit" value="Submit"> Submit </button>
      </form>
      <h4 class="w3-text-red">*all fields are required</h4>
    </main>
  </body>
</html>
