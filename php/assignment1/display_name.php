<!DOCTYPE HTML>
<html>
    <head><title>Display Full Name</title></head>
    <body>
        <h1>Fill in the below</h1>
        <div>
            <form action="display_name.php" method="post">
				<input type="text" name="first" placeholder="first name" required />
				<input type="text" name="middle" placeholder="middle name" required />
				<input type="text" name="last" placeholder="last name" required /><br><br>
				<input type="submit" value="submit" name="submit" />
			</form>
        </div>
        <div id="displayframe">
			<?php 
				if(isset($_POST["submit"])){
					$name= $_POST["first"]." ".$_POST["middle"]." ".$_POST["last"];
					echo "<h2> Welcome ".$name."</h2>"; 
				}
			?>
        </div>
    </body>
</html>