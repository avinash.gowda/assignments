<?php
echo "\n";
checkPrimeNumber($argv[1]);

function checkPrimeNumber($num) {
  if (is_numeric ($num) && intval ($num) == $num) {
    if (abs($num) == 1) {
      die ("One is neither prime nor composite\n");
    }
    for ($i=2; $i <= abs($num)/2 ; ++$i) {
      if($num%$i == 0) {
        die ("Given number,".$num." , is divisible by ".$i.", is not a prime\n");
      }
    }
    echo "Given number,".$num." , is a prime\n";
  } else {
    echo "Please enter a valid number\n.";
  }
}



?>
