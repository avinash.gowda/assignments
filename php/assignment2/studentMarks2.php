<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table border="1">
      <tr><th>name</th><th>physics</th><th>maths</th><th>chemistry</th><th>biology</th></tr>
      <?php

      $marks = [
          ['name' => 'mohammad',
              'physics' => 91,
              'maths' => 30,
              'chemistry' => 39,
          ],
          [
              'name' => 'qadir',
              'physics' => 35,
              'maths' => 30,
              'biology' => 39,
          ],
          [
              'name' => 'zara',
              'physics' => 35,
              'biology' => 30,
              'chemistry' => 39,
          ]
      ];

      foreach ($marks as $key) {
        echo "<tr>";
        $tablee = ['name' => '<td>-</td>', 'physics' => '<td>-</td>', 'maths' => '<td>-</td>', 'biology' => '<td>-</td>', 'chemistry' => '<td>-</td>' ];
        foreach ($key as $key => $value) {
          if (is_numeric($value)) {
            if ($value > 90) {
              $tablee[$key] = '<td style="color: green">'.$value.'</td>';
            } elseif ($value < 35) {
              $tablee[$key] = '<td style="color: red">'.$value.'</td>';
            } else {
              $tablee[$key] = '<td>'.$value.'</td>';
            }
          }else {
            $tablee[$key] = "<td>".$value."</td>";
          }
        }
        foreach ($tablee as $key => $value) {
          echo $value;
        }
        echo "</tr>";
      }

      ?>
    </table>
  </body>
</html>
